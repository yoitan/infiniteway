﻿#define DEBUG

using UnityEngine;
using System.Diagnostics;

public class AdDebug : MonoBehaviour {

	[Conditional("DEBUG")]
	public static void Log(string log) {
		UnityEngine.Debug.Log(log);
	}

	[Conditional("DEBUG")]
	public static void LogError(string log) {
		UnityEngine.Debug.LogError(log);
	}

	[Conditional("DEBUG")]
	public static void LogWarning(string log) {
		UnityEngine.Debug.LogWarning(log);
	}
}
