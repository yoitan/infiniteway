﻿using UnityEngine;
using System.Collections;

public class DcUtil {
	public static void removeAllChild(GameObject _rootObj) {
        if (_rootObj.transform.childCount > 0) {
            Transform target = _rootObj.transform.GetChild(_rootObj.transform.childCount-1);
			target.parent = null;
			MonoBehaviour.Destroy(target.gameObject);
		}

		if(_rootObj.transform.childCount > 0)
			removeAllChild(_rootObj);
	}
}
