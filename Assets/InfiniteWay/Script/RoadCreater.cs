﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoadCreater : MonoBehaviour {

    public GameObject roadBlock;
    public GameObject player;

    List<GameObject> listBlockPull;
    public List<GameObject> ListBlockPull
    {
        get
        {
            return listBlockPull;
        }
    }

	// Use this for initialization
	void Start () {
        for (int i=0; i<10; ++i) {
            GameObject block = Resources.Load("Prefabs/Block", typeof(GameObject)) as GameObject;
            listBlockPull.Add(block);
            Resources.UnloadUnusedAssets();

            block.GetComponent<BlockNode>().Distance = -1;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
