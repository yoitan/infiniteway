﻿//#define GOOGLE_NO_LOGIN
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
//using ANMiniJSON;
//using CodeStage.AntiCheat.ObscuredTypes;

public class DataManager : MonoBehaviour {
	public static DataManager instance;

	int score = 0;
	public int Score {
		get {
			return score;
		}
		set {
			score = value;
		}
	}

	public void initialize() {
		AdDebug.Log(gameObject.name + " initialize");
	}
	
	void Awake() {
		instance = this;
		DontDestroyOnLoad(gameObject);
		initialize();
	}

    void OnDestroy() {
    }
}
	                                     